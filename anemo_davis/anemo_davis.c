
#include <math.h>
#include "periph/gpio.h"
#include "xtimer.h"
#include "anemo_davis.h"


/* Anemometer physical interface and timer structure */
static struct {
    gpio_t mux_out;
    gpio_t mux_c;
    gpio_t mux_b;
    gpio_t mux_a;
    gpio_t counter_rst;
    gpio_t counter_n_en;
    uint64_t last_read_time_us;
} anemo_davis;


/* Prototypes */
static int read_rotations (void);
static int read_single_counter_output (int mux_c_val, int mux_b_val, int mux_a_val);
static int reset_and_enable_counter (void);
static int is_counter_reset (void);
static void set_address_bits (int mux_c_val, int mux_b_val, int mux_a_val);
static void wait_for_mux_output_propagation (void);
static void enable_counter_input (void);
static void disable_counter_input (void);
static void clear_counter (void);


/**
 * Init anemometer counter, reseet and enable
 */
int anemo_davis_init (
gpio_t mux_out, gpio_t mux_c, gpio_t mux_b, gpio_t mux_a,
gpio_t counter_rst, gpio_t counter_n_en)
{
    anemo_davis.mux_out = mux_out;
    anemo_davis.mux_c = mux_c;
    anemo_davis.mux_b = mux_b;
    anemo_davis.mux_a = mux_a;
    anemo_davis.counter_rst = counter_rst;
    anemo_davis.counter_n_en = counter_n_en;
    anemo_davis.last_read_time_us = 0;

    gpio_init (anemo_davis.mux_out, GPIO_IN);
    gpio_init (anemo_davis.mux_c, GPIO_OUT);
    gpio_init (anemo_davis.mux_b, GPIO_OUT);
    gpio_init (anemo_davis.mux_a, GPIO_OUT);
    gpio_init (anemo_davis.counter_rst, GPIO_OUT);
    gpio_init (anemo_davis.counter_n_en, GPIO_OUT);

    reset_and_enable_counter();

    return 0;
}


/**
 * Read and reset anemometer counter, while converting to kmh speed
 */
uint32_t anemo_davis_calc_speed_kmh_10e1 (void)
{
    /* Disable counter and save time */
    disable_counter_input();
    uint64_t read_time_us;

    read_time_us = xtimer_now_usec64();
    //printf("%lu, %lu\n", (uint32_t)read_time_us, (uint32_t)xtimer_now_usec64());

    int rotations = read_rotations();

    /* Enable counter and reset timer */
    reset_and_enable_counter();

    /* Calc speed in Km/h (official formula) */
    float speed_kmh =
        (rotations * SPEED_MULTIPLIER_KMH * U_SEC_IN_SEC) /
        (read_time_us - anemo_davis.last_read_time_us);

    /* Round to one decimal and multiply by 10 (vice versa) */
    uint32_t speed_kmh_10e1 = (uint32_t)(round(speed_kmh * 10));

    //printf("%f\n", (rotations * SPEED_MULTIPLIER_MPH * U_SEC_IN_SEC));
    //printf("%lu\n", (uint32_t)(read_time_us - anemo_davis.last_read_time_us));

    //uint32_t speed_kmh = 1;

    /*printf("%d, %f, %lu, %lu, %d, %lu\n",
        rotations,
        SPEED_MULTIPLIER_MPH,
        (uint32_t)read_time_us,
        (uint32_t)anemo_davis.last_read_time_us,
        U_SEC_IN_SEC,
        speed_kmh_10e1);*/

    anemo_davis.last_read_time_us = xtimer_now_usec64();

    return speed_kmh_10e1;
}


/**
 * Read number of rotations (counter value)
 */
static int read_rotations (void)
{

    int rotations = 0;

    rotations |= read_single_counter_output(0,0,0) << 0;
    rotations |= read_single_counter_output(0,0,1) << 1;
    rotations |= read_single_counter_output(0,1,0) << 2;
    rotations |= read_single_counter_output(0,1,1) << 3;

    rotations |= read_single_counter_output(1,0,0) << 4;
    rotations |= read_single_counter_output(1,0,1) << 5;
    rotations |= read_single_counter_output(1,1,0) << 6;
    rotations |= read_single_counter_output(1,1,1) << 7;

    //printf("Rotations: %d\n", rotations);

    /*printf("%d, %d, %d, %d, %d, %d, %d, %d\n",
        read_single_counter_output(0,0,0),
        read_single_counter_output(0,0,1),
        read_single_counter_output(0,1,0),
        read_single_counter_output(0,1,1),
        read_single_counter_output(1,0,0),
        read_single_counter_output(1,0,1),
        read_single_counter_output(1,1,0),
        read_single_counter_output(1,1,1)
    );*/

    return rotations;
}


/**
 * Read single counter output
 */
static int read_single_counter_output (int mux_c_val, int mux_b_val, int mux_a_val)
{
    set_address_bits(mux_c_val, mux_b_val, mux_a_val);
    wait_for_mux_output_propagation();

    return gpio_read (anemo_davis.mux_out);
}


/**
 * Set muliplexer address bis
 */
static void set_address_bits (int mux_c_val, int mux_b_val, int mux_a_val)
{
    gpio_write (anemo_davis.mux_c, mux_c_val);
    gpio_write (anemo_davis.mux_b, mux_b_val);
    gpio_write (anemo_davis.mux_a, mux_a_val);

    return;
}


/**
 * Reset and enable counter
 */
static int reset_and_enable_counter (void)
{
    set_address_bits(0,0,0);

    disable_counter_input();
    clear_counter();

    if (is_counter_reset() == -1) {
        printf("Error reseting anemo counter\n");
        return -1;
    }

    enable_counter_input();
    //anemo_davis.last_read_time_us = xtimer_now_usec64();

    return 0;
}


/**
 * Check if counter is reset by reading it
 */
static int is_counter_reset (void)
{
    if (read_rotations()) {
         return -1;
    }
    return 0;
}


/**
 * Set switch (1st MUX) to enable counter input
 */
static void enable_counter_input (void) {
    gpio_clear (anemo_davis.counter_n_en);
    wait_for_mux_output_propagation();
    return;
}


/**
 * Clear switch (1st MUX) to disable counter input
 */
static void disable_counter_input (void) {
    gpio_set (anemo_davis.counter_n_en);
    wait_for_mux_output_propagation();
    return;
}


/**
 * Clear (reset) counter
 */
static void clear_counter (void) {
    gpio_clear (anemo_davis.counter_rst);
    wait_for_mux_output_propagation();

    gpio_set (anemo_davis.counter_rst);
    wait_for_mux_output_propagation();

    gpio_clear (anemo_davis.counter_rst);
    wait_for_mux_output_propagation();

    return;
}


/**
 * Wait till MUX internally switches signals (should be less than 0.5us)
 */
static void wait_for_mux_output_propagation (void)
{
    xtimer_usleep(MUX_PROPAGATION_DELAY_US);
    return;
}
