#ifndef ANEMO_DAVIS_H
#define ANEMO_DAVIS_H

#include "periph/gpio.h"

#ifndef U_SEC_IN_SEC
#define U_SEC_IN_SEC                1000000
#endif

#define SPEED_MULTIPLIER_MPH        2.25
#define SPEED_MULTIPLIER_KMH        SPEED_MULTIPLIER_MPH * 1.609344

#define MUX_PROPAGATION_DELAY_US    1


/**
 * @brief   Init anemometer counter, reseet and enable
 *
 * @param[in]  mux_out          Multiplexer output pin
 * @param[in]  mux_c            Multiplexer select C pin
 * @param[in]  mux_b            Multiplexer select B pin
 * @param[in]  mux_a            Multiplexer select A pin
 * @param[in]  counter_rst      Counter reset pin
 * @param[in]  counter_n_en     Counter enable pin
 *
 * @returns     0 on success
 */
int anemo_davis_init (
    gpio_t mux_out, gpio_t mux_c, gpio_t mux_b, gpio_t mux_a,
    gpio_t counter_rst, gpio_t counter_n_en);

/**
 * @brief   Read and reset anemometer counter, while converting to kmh speed
 *
 * @returns     Wind speed in kmh
 */
uint32_t anemo_davis_calc_speed_kmh_10e1 (void);



#endif
