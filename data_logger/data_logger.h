/**
 * @{
 *
 * @todo       Add string formatting functions when writing, based on
 *             measurement data
 *
 * @file
 * @brief      Data logging script provides additional layer of abstraction
 *             over micro SD SPI driver. Takes care of both local data buffer
 *             and micro SD drivers.
 *
 */

#ifndef DATA_LOGGER_H
#define DATA_LOGGER_H

#include "sdcard_spi.h"

#define SD_BLOCK_CHECK_STR          "Anemo\n"
#define SD_BLOCK_CHECK_STR_LEN      6
#define NUM_OF_SD_BLOCKS            32000000
#define SD_READ_WRITE_BUFFER_SIZE   SD_HC_BLOCK_SIZE

/* Number of buffer separators (ex: 1 -> \n = 0x0A) */
#define NUM_OF_BUFFER_SEPARATORS    1

/**
 * @brief   Init data log
 *
 * Initialize SD card and search for previous Anemo measurement data. Set block
 * address number to next available block.
 *
 * @return                  0 on success, -1 on error
 */
int init_data_log (void);

/**
 * @brief   Write to data log
 *
 * Write to buffer (same size as SD card block, 512 bytes) and when full,
 * copy buffer contents to corresponding SD card block. Input data must be
 * smaller, then maximal available space in block (block size, minus check
 * string size)
 *
 * @param[in] external_buffer   pointer to write data
 * @param[in] len               write data length
 *
 * @return                  0 on success, -1 on error
 */
int write_data_log (char *external_buffer, int len);

#endif /* DATA_LOGGER_H */
/** @} */
