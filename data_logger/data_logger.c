#include <string.h>

#include "sdcard_spi.h"
#include "sdcard_spi_internal.h"
#include "sdcard_spi_params.h"

#include "data_logger.h"


/* RIOT built-in auto-init */
extern sdcard_spi_t sdcard_spi_devs[sizeof(sdcard_spi_params) / sizeof(sdcard_spi_params[0])];
sdcard_spi_t *card = &sdcard_spi_devs[0];

/* Put in static memory, no need for malloc(). Auto-init all 0. */
static char read_buffer[SD_READ_WRITE_BUFFER_SIZE];
static char write_buffer[SD_READ_WRITE_BUFFER_SIZE];

/* Index of fisrt available space in current block */
static uint32_t write_buffer_idx;

/* Current block address */
static uint32_t blockaddr;

/* Prototypes */
static void reset_write_buffer (void);
static void reset_write_buffer_idx (void);


int init_data_log (void) {

    if (sdcard_spi_init (card, &sdcard_spi_params[0]) != 0) {
        printf("SD card init error\n");
        return -1;
    }

    reset_write_buffer();
    reset_write_buffer_idx();

    sd_rw_response_t state;
    blockaddr = 0;

    while (blockaddr < NUM_OF_SD_BLOCKS) {
        sdcard_spi_read_blocks(card, blockaddr, read_buffer, 512, 1, &state);
        if (state != SD_RW_OK){
            printf("SD card read error %d\n", state);
            return -1;
        }
        if (strncmp(SD_BLOCK_CHECK_STR, read_buffer, SD_BLOCK_CHECK_STR_LEN) != 0) {
            printf("blockaddr: %lu\n", blockaddr);
            return 0;
        }
        blockaddr++;
    }

    return -1;
}


int write_data_log (char *external_buffer, int len) {
    printf("write_data_log\n");

    if (len > SD_READ_WRITE_BUFFER_SIZE) {
        printf("Buffer (%d) to large for block (%d) \n",
            len, SD_READ_WRITE_BUFFER_SIZE);
        return -1;
    }

    /* ex: '3 + 509 > 512' still fits into buffer (full) */
    if (len + write_buffer_idx > SD_READ_WRITE_BUFFER_SIZE) {
        sd_rw_response_t state;
        sdcard_spi_write_blocks(
            card, blockaddr, write_buffer, SD_READ_WRITE_BUFFER_SIZE, 1, &state);
        if (state != SD_RW_OK){
            printf("SD card write error %d\n", state);
            return -1;
        }
        reset_write_buffer();
        reset_write_buffer_idx();
        blockaddr++;
    }

    strcat(write_buffer, external_buffer);
    write_buffer_idx += len;

    printf("blockaddr: %lu, write_buffer_idx; %lu\n",
        blockaddr, write_buffer_idx);

    return 0;
}

/* Write zeroes to buffer and check string at beginning */
static void reset_write_buffer (void) {
    memset(write_buffer, 0, sizeof write_buffer);
    memcpy(write_buffer, SD_BLOCK_CHECK_STR, SD_BLOCK_CHECK_STR_LEN);
    return;
}

/* Set write index to first available space after check string (buffer empty) */
static void reset_write_buffer_idx (void) {
    //write_buffer_idx = SD_BLOCK_CHECK_STR_LEN + NUM_OF_BUFFER_SEPARATORS;
    write_buffer_idx = SD_BLOCK_CHECK_STR_LEN;
    return;
}
