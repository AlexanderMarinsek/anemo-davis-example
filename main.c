
/* Include from boards > arduino-zero > include */
#include "arduino_pinmap.h"
/* Include from drivers > include > periph */
#include "periph/gpio.h"
#include "periph/adc.h"
#include "xtimer.h"

#include <stdio.h>

#include "data_logger/data_logger.h"

#include <inttypes.h>

#include "anemo_davis/anemo_davis.h"

int main (void) {

	// LED
	gpio_init (ARDUINO_PIN_13, GPIO_OUT);

	printf("Start init\n");

	anemo_davis_init(
		GPIO_PIN(PA, 16),
		GPIO_PIN(PA, 22),
		GPIO_PIN(PA, 23),
		GPIO_PIN(PA, 24),
		GPIO_PIN(PA, 6),
		GPIO_PIN(PA, 4)
	);

	uint32_t speed = anemo_davis_calc_speed_kmh_10e1 ();

	while (1) {
		gpio_clear (ARDUINO_PIN_13);
		speed = anemo_davis_calc_speed_kmh_10e1 ();
		printf("Speed: %lu\n", speed);
		xtimer_usleep(5000000);

		gpio_set (ARDUINO_PIN_13);
		speed = anemo_davis_calc_speed_kmh_10e1 ();
		printf("Speed: %lu\n", speed);
		xtimer_usleep(5000000);
	}

	return 0;
}
